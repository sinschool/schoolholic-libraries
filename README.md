# README #

### How to use with Gradle ###

```
#!gradle
allprojects {
    repositories {
        mavenCentral()
        maven {
            url "https://api.bitbucket.org/1.0/repositories/sinschool/schoolholic-libraries/raw/snapshots"
        }
        maven {
            url "https://api.bitbucket.org/1.0/repositories/sinschool/schoolholic-libraries/raw/releases"
        }
    }
}

dependencies {
    compile "kr.co.schoolholic:android-helper:1.0.0"
}

```